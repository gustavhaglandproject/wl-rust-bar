use gtk::{gio, glib, glib::Object};

mod imp;
glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application,
        @implements gio::ActionMap;
}

impl Application {
    pub fn new() -> Self {
        Object::new(&[("date-format", &Some("%c"))]).expect("Failed to create application")
    }
}
