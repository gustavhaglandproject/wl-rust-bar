use std::cell::RefCell;

use gtk::subclass::prelude::*;
use gtk::{
    glib::{self, ParamFlags, ParamSpec, ParamSpecString},
    prelude::ToValue,
    subclass::prelude::GtkApplicationImpl,
};
use once_cell::sync::Lazy;

#[derive(Default)]
pub struct Application {
    date_format: RefCell<String>,
}

#[glib::object_subclass]
impl ObjectSubclass for Application {
    const NAME: &'static str = "Application";
    type ParentType = gtk::Application;
    type Type = super::Application;
}
impl ObjectImpl for Application {
    fn properties() -> &'static [ParamSpec] {
        static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
            vec![ParamSpecString::new(
                "date-format",
                "date-format",
                "Format of date time",
                Some("%c"),
                ParamFlags::READWRITE,
            )]
        });
        PROPERTIES.as_ref()
    }

    fn set_property(&self, _obj: &Self::Type, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
        match pspec.name() {
            "date-format" => {
                let format = value.get().expect("The value needs to be of type string");
                self.date_format.replace(format);
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
        match pspec.name() {
            "date-format" => self.date_format.borrow().to_value(),
            _ => unimplemented!(),
        }
    }
}

impl ApplicationImpl for Application {}

impl GtkApplicationImpl for Application {}
