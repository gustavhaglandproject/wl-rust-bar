use std::time::Duration;

use chrono::Local;
use glib::{clone, Continue};
use gtk::glib::{timeout_add_local, Char, OptionArg, OptionFlags};
use gtk::prelude::*;
use gtk::{glib, ApplicationWindow, Label};

mod wl_rust_bar;
use wl_rust_bar::Application;

fn main() {
    run()
}

fn run() {
    let app = Application::new();

    app.add_main_option(
        "date-format",
        Char::try_from('d').unwrap(),
        OptionFlags::NONE,
        OptionArg::String,
        "The format, how the date and time should be displayed",
        None,
    );

    app.connect_handle_local_options(move |a, b| match b.lookup::<String>("date-format") {
        Ok(result) => match result {
            Some(format) => {
                //TODO create a custom object with custom property to store date format without panicking
                a.set_property("date-format", format.as_str());
                -1
            }
            None => -1,
        },
        Err(err) => {
            println!("{}", err.to_string());
            1
        }
    });

    app.connect_activate(build_ui);

    app.run();
}

fn build_ui(app: &Application) {
    let label = Label::builder()
        .label(
            &Local::now()
                .format(app.property::<String>("date-format").as_str())
                .to_string(),
        )
        .build();

    timeout_add_local(
        Duration::from_millis(1000),
        clone!(@weak label, @weak app => @default-return Continue(false), move || {
            label.set_text(&Local::now().format(app.property::<String>("date-format").as_str()).to_string());
            Continue(true)
        }),
    );

    let window = ApplicationWindow::builder()
        .application(app)
        .child(&label)
        .build();

    gtk_layer_shell::init_for_window(&window);
    gtk_layer_shell::set_anchor(&window, gtk_layer_shell::Edge::Top, true);
    gtk_layer_shell::set_layer(&window, gtk_layer_shell::Layer::Bottom);
    gtk_layer_shell::auto_exclusive_zone_enable(&window);

    window.show_all();
}
